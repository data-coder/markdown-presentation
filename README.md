# Data Coder - Markdown Presentation

A project to store and build Markdown Presentation linked to my [blog Data Coder](https://datacoder.substack.com).

## Description
The folder `/presentatiosn` contains subfolders, one for each presensation. Inside each folder you can find one markdown file, and the ressources used (most of the time pictures).

The folder `/themes` contains all custom theme CSS

## Visuals

## Installation
- Add [Marp plugin](https://marp.app/#get-started) on [VS Code](https://code.visualstudio.com/download)

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.


## Authors and acknowledgment
Cédric Le Penmelen
Special thanks to my mentor, Stéphane WALTER, who made me discover the Marp tool.

## License
MIT

